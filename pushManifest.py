#!/usr/bin/env python
#
# Push a list of files using a manifest to a remote site from a local directory.
# Provides a set of optional methods.
#
# Note that this depends on an ssh-agent being available, and will raise an
# error if not ...
#

import os,sys
from time import time
from datetime import datetime

from ftutils import kB,MB,GB,TB,KiB,MiB,GiB,TiB
from ftutils import humanBW,humanSize,bwtovpd
from simpleThreader import doit
from checkm import getFilesFromManifest

overallStreams=4

pmax=50200

bbcp='bbcp -v -z -F -k -s %%s -a --port 50000:%s '%pmax
scp= 'scp '
rsync= 'rsync -e ssh -a --partial '
    
def pPushFile(args):
    ''' This is the parallel invocation of pushFile.'''
    return pushFile(args[0],args[1],args[2],args[3],args[4],logfile=args[5])
    
def pushFile(method,localFile,remoteDir,remoteHost,remoteUser,logfile=None):
    ''' Move the localFile to the remoteDir '''
    t0=time()
    cmd='%s %s %s@%s:%s/'%(method,localFile,remoteUser,remoteHost,remoteDir)
    if logfile is not None:
        f=open(logfile,'a')
        f.write(cmd)
        f.close()
    os.system(cmd)
    t1=time()
    dt=t1-t0
    osize=os.path.getsize(localFile)
    bw=osize/dt
    date=datetime.fromtimestamp(t1)
    size,units=humanSize(osize)
    bw,bwunits=humanBW(bw)
    logstring='%s; Pushed %s to %s at %s %s \n'%(date,localFile,remoteHost,bw,bwunits)
    print logstring
    if logfile is not None:
        f=open(logfile,'a')
        f.write(logstring)
        f.close()
    return osize,dt
        
def methodChoose(option,args=None):
    ''' Return a method string for file transfer '''
    choices={'bbcp':bbcp,'scp':scp,'rsync':rsync}
    assert option in choices
    choice=choices[option]
    if choice=='bbcp' and args is None: args=(2,)
    if args is None: 
        return choice
    else: return choice%args
    

if __name__=="__main__":
    #
    try:
        manifest=sys.argv[1]
        remoteDir=sys.argv[2]
        remoteHost=sys.argv[3]
        remoteUser=sys.argv[4]
        method=sys.argv[5]
        if len (sys.argv) > 6: 
            args=tuple(sys.argv[6:])
        else: args=None
    except:
        print 'Usage: pushManifest manifile remoteDir remoteHost remoteUser, method, methodargs '
        print 'You entered ',sys.argv
        exit()
        
    agent=os.getenv('SSH_AGENT_PID')
    if agent is not None:
        print 'SSH agent found at pid %s'%agent
    else:
        raise ValueError('No agent found, unable to continue')
        
    # use manifest input directory
    files=getFilesFromManifest(manifest)
 
    method=methodChoose(method,args)
    
    args=[(method,f,destDir,host,user,'pTransfer.log') for f in files]
    print 'Parallel movement begins for %s'%files
    print 'Pushing to %s using %s'%(remoteHost,sys.argv[5:])
    t0=time()
    results=doit(pPushFile,args,nstreams=overallStreams)
    print 'Parallel movement ends'
    t1=time()
    tsize=0
    for o,i in results:
        tsize+=o
    tbw=tsize/(t1-t0)
    tbw,bwunits=humanBW(tbw)
    tsize,units=humanSize(tsize)
    print 'Moved %s (%s %s) at %s %s\n'%(manifest,tsize,units,tbw,bwunits)
   
        
