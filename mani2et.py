#!/usr/bin/env python
#
# man2et.py
# used for writing a manifest of files to the JASMIN elastic tape facility
# elastic tape API information copied from et_put.py 18/02/14
#

import sys
import os
from checkm import checkmMF
import StringIO
import ConfigParser
import logging
#import getpass
import errno

#### 
# Set up logging in the same way as the elastic tape API does it
# Create a variable to represent the logger, this will be configured to 
# send logs to a file if logging has been chosen.
#
logLevel = logging.DEBUG
defaultHandler = logging.StreamHandler(sys.stdout)
logFormat = '[%(asctime)s] %(levelname)s %(name)s: %(message)s'
defaultFormatter = logging.Formatter( fmt=logFormat )
defaultHandler.setFormatter( defaultFormatter )
logger = logging.getLogger( 'StorageD' )
logger.addHandler( defaultHandler )
logger.setLevel( logLevel )

### elastic tape API
# Import elastic_tape client library after logging is set up
import elastic_tape.client
from elastic_tape.shared.error import StorageDError

# Default configuration global variables
conn = None
host = ''
port = 7456
delete = False
#requestor = getpass.getuser()
workspace = ''
name = 'testing'

# Ported from et_put.py
def readConfig():
    ''' Reads an elastic tape configuration file (.et_config) from 
    user home directory '''

    global host,port, workspace

    configName = '%s/.et_config'%(os.environ['HOME'])
    confObj = ConfigParser.SafeConfigParser()
    
    readList = confObj.read( [ '/etc/et_config', configName ] )
    if len(readList) == 0:
        usage ('No config file found in either %s or %s'%( configName,'/etc/et_config' ) )
    try:
        host = confObj.get('DEFAULT','Host')
        port = confObj.getint('DEFAULT','Port')
        workspace = confObj.get('Batch','Workspace')
    except ConfigParser.Error,e:
        logger.error('Caught error %s when trying to read config file %s\n'%(e,configName))

# manifest specific
def mani2gen(manifest_filename,manifest_instance):
    
    ''' Given a manifest, get the file names, and put them into a
    suitable generator object for et_put. (We are mimicing the
    generator which et_put creates when it adds a directory.) '''

    #need fully qualified file names

    fpath=os.path.abspath(manifest_filename)
    fdir,ffile=os.path.split(fpath)
    print 'Assuming manifest files in same directory [%s] as manifest'%fdir
    
    listOfFiles=[fpath,]
    
    for f in manifest_instance.files:
        yield(os.path.join(fdir,str(f)))
    
# manifest specific version of et_put function sendList
def sendMani(manifest_name,fswalked):
    
    ''' Manifest equivalent of sendList, sends an open file of 
    filenames to the elastic tape client '''
    
    global conn
    debug=False
    assert host != ''
    if debug:
        logger.debug('Skipping actual tape client')
        logger.debug('Would have written:')
	print fswalked
        for l in fswalked: logger.debug(l)
        logger.debug('those!')
        batchID=1
    else:
        conn = elastic_tape.client.connect( host, port )
        batch = conn.newBatch( workspace, name )
        batch.contents.append( fswalked )
        logger.debug('Sending files listed in %s'%manifest_name)
        batchID = batch.register()
        logger.debug('Sent file details successfully')
    return batchID
    
def elasticLogger(level=logging.DEBUG,logDir=None,logFile='man2et.log'):
    ''' Sets up the elastic tape logger '''
    loglevel=level
    if logDir is None:
        logDir=os.getcwd()
        try:
            os.makedirs( logDir )
        except OSError,e:
            if e.errno == errno.EEXIST:
                pass
            else:
                usage('Could not create directory %s for logfile' %(logDir) )
        if not os.access( logDir, os.W_OK ):
            usage('No permission to write to directory %s for logfile'%(logDir))
        
        # ok setup logger
        logFile=os.path.join(logDir,logFile)
        h = logging.FileHandler( logFile )
        h.setFormatter( defaultFormatter )
        logger.addHandler( h )
        logger.removeHandler( defaultHandler )
        logger.setLevel( logLevel )

def usage(extraInfo=None):
    ''' Print usage string and exit with error'''
    usage='Usage: mani2tape manifest_filename'
    sys.stderr.write(usage)
    if extraInfo is not None: print extraInfo
    sys.exit(1)

if __name__=="__main__":
    
    # setup error handling
    args=sys.argv
    if len(args)!=2:
        print 'Incorrect number of arguments'
        usage()
    manifile=args[1]
    if not os.path.exists(manifile):
        print 'Manifest File not found'
        usage()
    mf=checkmMF()
    try:
        mf.read(manifile)
    except:
        usage('manifest_filename must point to a valid checkm manifest file')
        
    # elastic tape configuration
    readConfig()
    
    # further log configuration
    elasticLogger()
    
    # get files for elastic tape
    fswalked=mani2gen(manifile,mf)
    
    #send to elastic tape
    batchID=sendMani(manifile,fswalked)
    
    sys.stdout.write('Batch ID: %d\n'%(batchID))

    
    
   
    
    
    
    
    
