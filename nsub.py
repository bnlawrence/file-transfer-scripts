#!/usr/bin/env python
# This small python script simplifies large numbers of jug queue submissions
# Basic usage
#   nsub.py 20 queuing_command jobscript logtemplate
#   e.g 
#   nsub.py 20 'bsub -q lotus' /home/users/lawrence/bashit.sh '/home/users/lawrence/jug%s.log'
import os,sys
def nsub(i,maxI,qCmd,bCmd,logTemplate):
    ''' Do actual queue submission '''
    n=len(str(maxI))
    idstr='{0:0>{width}}'.format(i,width=n)
    try:
        logFile=logTemplate%idstr
    except:
        print 'Invalid template?'
        print "Need something like 'blah%s.log' "
        raise
    scmd='%s -o %s %s'%(qCmd,logFile,bCmd)
    print 'Executing : '+scmd
    os.system(scmd)
if __name__=="__main__":
    args=sys.argv
    print args
    if len(args)!=5:
        print '''
        Basic usage
            nsub.py 20 queuing_command jobscript logtemplate
            e.g 
            nsub.py 20 'bsub -q lotus' /home/users/lawrence/bashit.sh '/home/users/lawrence/jug%s.log'
        '''
        exit()
    maxI=int(args[1])
    for i in range(maxI):
        nsub(i,maxI,args[2],args[3],args[4])
    

