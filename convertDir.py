# This script is designed to do a fields file to pp conversion for 
# an archer directory, and move the associated data to the RDF in the process.
#
import os
import glob
import sys
import uuid
from time import sleep,time
import stat
from simpleThreader import doit

fields2pp='/work/n02/n02/hum/bin/ff2pp'
maxQueuedJobs=10

qtemplate='''
#!/bin/bash 
#
#PBS -l select=serial=true
#PBS -l walltime=0:5:0
#PBS -A n02
infile=%s
outfile=%s
date1=$(date +"%%s")
%s $infile $outfile
date2=$(date +"%%s")
diff=$(($date2-$date1))
echo "Converting $infile took ${diff}s." >> queue.log
du ${infile}
'''

def getFiles(d,t):
    ''' Get the full path for a bunch of files in a directory '''
    p=os.path.join(d,t)
    files=glob.glob(p)
    # now make sure none of them are already pp files
    okfiles=[]
    for f in files:
        p,e=os.path.splitext(f)
        if e !='.pp': 
            okfiles.append(f)
        else: print 'Avoiding '+f
    return okfiles
    
def checkqueue():
    ''' See if it's ok to submit more jobs '''
    toomany=True    
    while (toomany):    
       os.system('qstat -u bnl > queued.txt')
       f=file('queued.txt','r')
       lines=f.readlines()
       if len(lines)<5+maxQueuedJobs: toomany=False
       if toomany: sleep(10)

def convertFile(args,q_it=False):
    t0=time()
    f,destDir,count,ufname=args
    ff=os.path.split(f)
    fn=os.path.join(destDir,ff[1]+'.pp')
    if os.path.exists(fn):
        print 'Skipping %s'%fn
    else:
        qj=qtemplate%(f,fn,fields2pp)
        bj='convert_%s_%s.sh'%(ufname,count)
        bjf=open(bj,'w')
        bjf.write(qj)
        bjf.close()
        count+=1
        if q_it:
            os.system('qsub %s'%bj)
            checkqueue()
        else: 
            qf='%s'%bj
            os.chmod(qf, stat.S_IRWXU | stat.S_IXGRP)  # make executable
            os.system(qf)  # execute
            os.remove(qf)  # and then we should know it's done ...
    t1=time()
    return [count,fn,t1-t0]

if __name__=="__main__":
    # get the directory
    try:
        directory=sys.argv[1]
        template=sys.argv[2]
        destDir=sys.argv[3]
    except:
        print '''
        Usage: python convertDir directory template destDir        
            where
        template is something like 'xjarpa.pk*'
        
        e.g
        
        python convertDir './' 'xjarpa.pk*' /nerc/n02/n02/bnl/xjarp 
        
        '''
        exit()
        
    if directory[0]!='/':
        wd=os.getcwd()
        directory=os.path.join(wd,directory)
    assert os.path.isdir(directory)
    
    #get a list of files to work with
    files=getFiles(directory,template)
    
    #sort out the destination
    if not os.path.exists(destDir):
        os.makedirs(destDir)
        assert os.path.isdir(destDir)
    
    #create a unique base for the jobs
    ufname=str(uuid.uuid4())
    count=0
    
    print 'Processing ',files
    
    count=range(len(files))
    args=[[files[i],destDir,i,ufname,outManifest] for i in count]
    
    results=doit(convertFile,args)
    
    
