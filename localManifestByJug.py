#
# Jug parallelisable script for creating a local checksum
# manifest to match a remote checksum manifest

from checkm import checkmMF,checkmFile
import os
from jug import TaskGenerator,value,barrier
import socket

@TaskGenerator
def jugableMakeCheckmFile(filename):
    ''' This is a simple wrapper to checkm which exists so jug can
    wrap it up as a jug task '''
    x=checkmFile(filename=filename)
    return x

def finaloutput(remoteManifest,rmf,outputs,localManifest):
    ''' This writes out the final results '''
    lmf=checkmMF()
    lmf.append('Manifest created for files in Remote Manifest %s'%remoteManifest)
    lmf.append('Original Manifest Comments Follow')
    for c in rmf.comments[1:]:
        lmf.append('->%s'%c)
    lmf.append('Local Manifest Comments Follow')
    lmf.append('Running on: %s'%socket.getfqdn())
    for f in outputs: lmf[f[0]]=f
    lmf.append(lmf.now())
    comment='Total volume of %s files in manifest: %s %s '%(len(lmf),lmf.hsize[0],lmf.hsize[1])
    print comment
    lmf.append(comment)
    lmf.write(localManifest)
    
# read the arguments to this from an input file since we can't
# use the command line arguments (intercepted by jug).

inputs='maniArguments.txt'
try:
    arguments=open(inputs,'r').readlines()
except:
    print "Set up expects a file called %s to exist"%inputs
    raise
if len(arguments)!=2:
    print "%s should contain two lines:"%inputs
    print "The remote manifest file name, and"
    print "The local manifest file name"
    raise ValueError('Invalid Input')
arguments=[a.strip('\n') for a in arguments]    
remoteManifest,localManifest=tuple(arguments)

#
# Load remote manifest
rmf=checkmMF()
rmf.read(remoteManifest)

# get going now
print rmf.files
localcheckedFiles=[jugableMakeCheckmFile(f) for f in rmf.files]

barrier()

# now pick up the results
outputs=value(localcheckedFiles)
finaloutput(remoteManifest,rmf,outputs,localManifest)








        

