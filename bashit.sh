#BSUB -e %J.e
# Users would need to construct their own versions of this.
# Basically you need to ensure the local working directory is what you want
# and then execute the jug execute statement ... inside each job.
cd /group_workspaces/jasmin/hiresgw/xjanp
jug_py27 execute localManifestByJug.py
