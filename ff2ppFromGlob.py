#!/usr/bin/env python
import sys
import os
import uuid
from convertDir import convertFile
import stat
from simpleThreader import doit
import glob
from checkm import checkmManifest,makeCheckmFile

if __name__=="__main__":
    # get the manifest
    try:
        template=sys.argv[1]
        destDir=sys.argv[2]
        streams=int(sys.argv[3])
        manibase=sys.argv[4]
        print len(sys.argv)
    except:
        print '''
        Usage e.g: ff2ppFromGlob.py 'xjanp.p*' destDir streams manibase
        where
         - 'xjanp.p*' is a template describing which files you want to process    
         - destDir is where the output should go
         - streams is an integer denoting the amount of parallellisation to use
         - manibase is the basefile template for input and output manifests.
        '''
        raise
        exit()
        
    #get a list of files to work with
    files=glob.glob(template)
    
    #create a formal manifest of those files
    filesIn=manibase+'.in.cmmf'
    mfin=checkmManifest(filesIn,'w')
    mfin.addComment('Input files for ff2ppFromGlob')
    mfin.addComment('Current working directory %s'%os.getcwd())
    p,f=os.path.split(files[0])
    mfin.addComment('Input Directory %s'%p)
    #parallelise the checksums:
    cfiles=doit(makeCheckmFile,files,nstreams=streams)
    # ok now load it up
    for f in cfiles: 
        mfin.addCheckmFile(f)
    mfin.mclose()
    
    #sort out the destination
    if not os.path.exists(destDir):
        os.makedirs(destDir)
        assert os.path.isdir(destDir)
    
    #create a unique base for the jobs
    ufname=str(uuid.uuid4())
    count=0
    
    print 'Processing ',files
    
    count=range(len(files))
    args=[[files[i],destDir,i,ufname] for i in count]
    
    results=doit(convertFile,args,nstreams=streams)
    print results
    ofiles=[]
    for r in results:
        ofiles.append(r[1])
    
    # now create the output manifest
    filesOut=manibase+'.out.cmmf'
    mfo=checkmManifest(filesOut,'w')
    mfo.addComment('Output files for ff2ppFromGlob')
    mfo.addComment('Current working directory %s'%os.getcwd())
    mfo.addComment('Output directory %s'%destDir)
    #parallelise the checksums:
    cfiles=doit(makeCheckmFile,ofiles,nstreams=streams)
    # ok now load it up
    for f in cfiles: mfo.addCheckmFile(f)
    mfo.mclose()
