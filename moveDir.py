#!/usr/bin/env python
# Move a list of files using a manifest from a remote site to a local directory.
# Provides a set of optional methods.

import os,sys
from time import time
from datetime import datetime

from ftutils import kB,MB,GB,TB,KiB,MiB,GiB,TiB
from ftutils import humanBW,humanSize,bwtovpd
from simpleThreader import doit
from checkm import getFiles
from getSizeDifManifest import getSizeDifManifest

overallStreams=4

bbcp='bbcp -v -z -F -k -s %s -a --port 50000:50300 '
scp= 'scp '
rsync= 'rsync -e ssh -a --partial '
    
def pPullFile(args):
    ''' This is the parallel invocation of pullFile.'''
    return pullFile(args[0],args[1],args[2],fromHost=args[3],fromUser=args[4],logfile=args[5])
    
def pullFile(method,remoteFile,localDir,fromHost='192.168.17.2',fromUser='bnl',logfile=None):
    ''' Move the files listed in targetFiles fromDir to the current directory '''
    t0=time()
    cmd='%s %s@%s:%s %s'%(method,fromUser,fromHost,remoteFile,localDir)
    if logfile is not None:
        f=open(logfile,'a')
        f.write(cmd)
        f.close()
    # initialise outputs
    p,f1=os.path.split(remoteFile)
    logstring='Attempting to pull %s'%f1
    osize,dt=0,0
    # execute file pull:
    rv=os.system(cmd)
    # We have to handle errors cunningly, it turns out
    # that multiprocessing handles errors rather poorly,
    # and we need to somehow catch the system command ...
    if rv==0:
        t1=time()
        dt=t1-t0
        np=os.path.join(localDir,f1)
        try:
            osize=os.path.getsize(np)
            bw=osize/dt
            date=datetime.fromtimestamp(t1)
            size,units=humanSize(osize)
            bw,bwunits=humanBW(bw)
            logstring='%s; Got %s (%s %s) at %s %s\n'%(date,f1,size,units,bw,bwunits)
        except Exception,e:
			e=str(Exception(e))
			logstring='Failed to get %s (%s)'%(f1,e)
	else:
		e='Error - %s - returned by system'%rv
		logstring='Failed to get %s (%s)'%(f1,e)
		
    print logstring
    if logfile is not None:
        f=open(logfile,'a')
        f.write(logstring)
        f.close()
    return osize,dt
        
def methodChoose(option,args=None):
    ''' Return a method string for file transfer '''
    choices={'bbcp':bbcp,'scp':scp,'rsync':rsync}
    assert option in choices
    choice=choices[option]
    if choice=='bbcp' and args is None: args=(2,)
    if args is None: 
        return choice
    else: return choice%args
    

if __name__=="__main__":
    #
    # Will normally need to source the agent first ...
    #
    try:
        manifest=sys.argv[1]
        sourceDir=sys.argv[2]
        destDir=sys.argv[3]
        host=sys.argv[4]
        user=sys.argv[5]
        method=sys.argv[6]
        if len (sys.argv) > 7: 
            args=tuple(sys.argv[7:])
        else: args=None
    except:
        print 'Usage: moveMani.py remoteManifest sourceDir destDir host user method (methodargs ...) '
        print '(You should manually override input directory information in manifest, if it exists)'
        print 'You entered ',sys.argv
        exit()
        
    # work out what we actually need to get
    sizeDifManifest=getSizeDifManifest(manifest)
    files=getFiles(sizeDifManifest,sourceDir)
    
    # now go for it
    method=methodChoose(method,args)
    args=[(method,f,destDir,host,user,'pTransfer.log') for f in files]
    print 'Parallel movement begins for %s'%files
    print 'Pulling from %s using %s'%(host,sys.argv[6:])
    t0=time()
    results=doit(pPullFile,args,nstreams=overallStreams)
    print 'Parallel movement ends'
    t1=time()
    tsize=0
    for o,i in results:
        tsize+=o
    tbw=tsize/(t1-t0)
    tbw,bwunits=humanBW(tbw)
    tsize,units=humanSize(tsize)
    print 'Got %s (%s %s) at %s %s\n'%(manifest,tsize,units,tbw,bwunits)
   
        
