#!/usr/bin/env python
import sys, os
qtemplate='''#!/bin/bash 
#
#PBS -l select=serial=true
#PBS -l walltime=%s:0:0
#PBS -A n02
#
# need files to be written in submitting directory (if no path specified)
cd $PBS_O_WORKDIR
#
template='%s'
streams=%s
maniName=%s
date1=$(date +"%%s")
maniFromGlob.py "$template" $streams $maniName
date2=$(date +"%%s")
diff=$(($date2-$date1))
echo "Making manifest for $template ($maniName) took ${diff}s." >> globQueue.log
'''

def makeQfile(hoursNeeded,template,streams,maniName):
    qj=qtemplate%(hoursNeeded,template,streams,maniName)
    p,e=os.path.splitext(maniName)
    p,f=os.path.split(p)
    qfn='maniMake_%s.sh'%f
    qf=open(qfn,'w')
    qf.write(qj)
    qf.close()

if __name__=="__main__":
    try:
        globtemplate=sys.argv[1]
        streams=int(sys.argv[2])
        maniName=sys.argv[3]
        hoursNeeded=sys.argv[4]
    except:
        print 'Usage: submitMakeMani.py globtemplate nstreams manifilename hoursNeeded'
        exit()
    makeQfile(hoursNeeded,globtemplate,streams,maniName)
