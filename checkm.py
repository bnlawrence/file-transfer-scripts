import subprocess
import os
import datetime
import unittest
import glob
import tempfile
import time
import fnmatch
from ftutils import humanSize

COLUMNS = { 0:"SourceFileOrURL",
            1:"Alg",
            2:"Digest",
            3:"Length",
            4:"ModTime",
            5:"TargetFileOrURL",
            }
            
def cmd2result(cmdstr,logfile=None):
    ''' Execute a command in the shell, and return the result, with error handling'''
    def getoutput(*popenargs, **kwargs):
        if 'stdout' in kwargs:
            raise ValueError('stdout argument not allowed, it will be overridden.')
        process = subprocess.Popen(stdout=subprocess.PIPE, *popenargs, **kwargs)
        output, unused_err = process.communicate()
        retcode = process.poll()
        if retcode:
            cmd = kwargs.get("args")
            if cmd is None:
                cmd = popenargs[0]
            raise subprocess.CalledProcessError(retcode, cmd)
        return output
    return getoutput(cmdstr,shell=True)
    #Python 2.7 ready ...
    #try:
    #    return check_output(cmdstr,shell=True)
    #except CalledProcessError as e:
    #    estring='[%s] execution failed %s'%(cmdstr,e)
    #    if logfile is not None: logfile.write(estring+'\n')
    #    raise 
        
def checksum(filename,algorithm='sha1'):
    ''' Return the checksum for a given filename '''
    try:
        method={'sha1':'sha1sum','md5':'md5sum'}[algorithm]
    except KeyError:
        raise ValueError('Unrecognised checksum algorithm: %s'%algorithm)
    ondisk=cmd2result('%s %s'%(method,filename))
    return ondisk.split(' ')[0]

def makeCheckmFile(filename):
    ''' given a filename, make and return a checkmFile instance '''
    # convenience functional form for parallel invocation
    x=checkmFile(filename=filename)
    return x
    
def aggregateMF(listofManifests,comments=[]):
    ''' Take a list of manifest file names and aggregate them, with
    whatever comments are provided heading the aggregation, and return
    the aggregated manifest (which is not written out internally!)'''
    newMF=checkmMF()
    newMF.append('Aggregation of manifest files by aggregateMF')
    for c in comments: newMF.append(c)
    n=len(str(len(listofManifests)))
    index=0
    for m in listofManifests:
        index+=1
        thisone=checkmMF()
        thisone.read(m)
        idstr='{0:0>{width}}'.format(index,width=n)
        for c in thisone.comments[1:]:
            newMF.append('A%s:%s:'%(idstr,c))
        flist=''
        for f in thisone.files:
            flist+=f+','
            newMF[f]=thisone[f]
        newMF.append('A%s%s:'%(idstr,flist[0:-1]))
    newMF.append(newMF.now())
    newMF.apsize()
    return newMF
            
class checkmFile(object):
    ''' A file as seen by checkm '''
    def __init__(self,filename=None,tokens=None,line=None):
        ''' Instantiate from disk using a filename, or
                        from a list of tokens, or
                        from a formal checkm text line '''
        self.tokens=None
        options=0
        if filename is not None:
            self.fromDisk(filename)
            options+=1
        if tokens is not None:
            self.fromTokens(tokens)
            options+=1
        if line is not None:
            self.fromLine(line)
            options+=1
        if options>1: 
            raise ValueError('Enter only one method to instantiate a checkm record')
        self.keys={}
        for v in COLUMNS:
            self.keys[COLUMNS[v]]=v
    def fromTokens(self,tokens):
        ''' Instantiate with a tuple or list corresponding to the checkm entries '''
        self.tokens=list(tokens)
        self.validate()
    def fromDisk(self,filename):
        assert os.path.isfile(filename)
        ondisk=checksum(filename)
        mtime=os.path.getmtime(filename)
        dtime=datetime.datetime.fromtimestamp(mtime)
        self.tokens=[str(i) for i in 
            filename,'sha1',ondisk,os.path.getsize(filename),dtime,' ']
        self.validate()
    def fromLine(self,line):
        ''' Instantiate from checkm text string '''
        self.tokens=line.split('|')[1:-1]
        self.validate()
    def __len__(self):
        ''' Return length if known, else None '''
        try:
            s=int(self['Length'])
            return s
        except TypeError:
            return -1
    def validate(self,fromDisk=False,sizeOnly=False):
        ''' Validate a checkm line. The default is simply to check the format of the text
        string found in a manifest, however, if fromDisk is True, the disk file is checksummed
        and compared to the manifest (unless sizeOnly is True, in which case the file size on
        disk is checked and compared to the manifest) '''
        if len(self.tokens)!=6:
            raise ValueError('Candidate CHECKM tuple [%s] does not contain 6 items'%self.tokens)
        if fromDisk:  
            try:
                assert os.path.isfile(self.tokens[0])
            except:
                raise ValueError('%s is not a recognised file '%self.tokens[0])
            if sizeOnly:
                return os.path.getsize(self.tokens[0])==len(self)
            else: 
                ondisk=checksum(self.tokens[0],algorithm=self.tokens[1])
                return ondisk==self.tokens[2]
        else:
            if not sizeOnly: 
                return True
            else:
                raise ValueError('Validation cannot be carried out with sizeOnly=T and fromDisk=F')
    def __str__(self):
        return '|%s|'%'|'.join(self.tokens)
    def __repr__(self):
        return self.__str__()
    def __cmp__(self,other):
        return self.tokens==other.tokens
    def __getitem__(self,value):
        if value not in self.keys:
            return ValueError('%s not a valid key into checkmFile'%value)
        return self.tokens[self.keys[value]]
    def __setitem__(self,key,value):
        if key not in self.keys:
            return ValueError('%s not a valid key into checkmFile'%value)
        self.tokens[self.keys[key]]=str(value)
            
class checkmMF(object):
    ''' A complete checkm manifest, including operators.
    Usage:
        x=checkmMF()
        x.append('A comment')      - appends a comment
        x[fname]=checkmInstance    - adds/replaces a checkmFile instance
        x.add(fname)               - parses fname to a checkmFile and adds
        x-y      - will return a manifest of all files which appear in x and not in y
        x.files - returns a list of filenames.
        x.size  - returns size of payload files in bytes
                - if one or more files has an unknown size, returns None
        x.hsize - returns a human readable payload size
                - if one or more files has an unkown size, returns None
        y=x[fname] returns a checkmFile if it exists
        x.comments returns a list of all comments
        x.comments=[1,2,3] replaces all comments 
        x.comments.append([1,2,3]) appends those comments
        len(x) returns number of files in manifest
        str(x) returns a string view
        z=x+y will join all distinct y comments after the x ones, and
        then aggregate the list of files returning a new mf
        x==y: will return true if the list of files is the same,
              and they have the same checksums and length ... 
              (anything else can differ).
        x.write(filename) will write the manifest to disk
        x.read(filename) will read a manifest from disk
        x.now() get a usefultimestamp for provenance recording.
        x.apsize() will update the manifest comments with it's current size
        x.subset('template') will produce a subset manifest corresponding to the glob,
        carrying the same metadata
    '''
    version='%checkm_0.7'
    def __init__(self,initialComments=None):
        ''' Instantiate with or without initial comments '''
        self.__emptyme()
        if initialComments!=None:
           self.comments+=initialComments

    def __emptyme(self):
        ''' Empty the manifest '''
        self.content={}
        self.files=[]
        self.size=0
        self.hsize=(0,'B')
        self.comments=[self.version]
    
    def append(self,comment):
        ''' Append a single comment '''
        self.comments.append(comment)
        
    def apsize(self):
        ''' Append current size details '''
        comment='Total volume of %s files in manifest: %s %s '%(len(self.files),self.hsize[0],self.hsize[1])
        self.append(comment)
    
    def __setitem__(self,key,value):
        ''' Set a file value in the manifest '''
        self.content[key]=value
        if key not in self.files: self.files.append(key)
        # now muck around with the consequent changes to manifest
        # length properties
        s=len(value)
        if s <0:
            if self.size >0: 
                self.size=-1  # this new entrant has broken it
            else:
                pass # do nothing it's already broken.
        else:
            if self.size <0:
                # We should check this wasn't the joker in the pack
                # but meanwhile, we do nothing, #FIXME
                pass 
            else:
                self.size+=s
        if self.size>0:
            self.hsize=humanSize(self.size)
        else: 
            self.hsize=None
    def __getitem__(self,key):
        return self.content[key]
    def __len__(self):
        ''' Return number of files in manifest '''
        return len(self.files)
    def add(self,fname):
        ''' Parse a file off the disk, and add '''
        cf=checkmFile(filename=fname)
        self[fname]=cf
    def __str__(self):
        ''' Return a string view, which is suitable for printing '''
        s=''
        for c in self.comments:
            s+='#%s\n'%c
        for f in self.files:
            s+='%s\n'%self[f]
        return s
    def __eq__(self,other):
        ''' test equality '''
        if len(self)!=len(other): return False
        for f in self.files:
            if f not in other.files: return False
            if self[f]['Digest']!=other[f]['Digest']: return False
        return True
    
    def __sub__(self,other):
        ''' Return a list of checkmFiles which appear in self, but
        do not appear in other with the same checksum. Raise a
        ValueError if there are files in other which do not appear
        in self '''
        results=[]
        for f in other.files:
            if f not in self.files: 
                raise ValueError(
                    'Second checkmManifest contains unexpected files')
        for f in self.files:
            if f not in other.files: 
                results.append(self[f])
            else:
                if self[f]['Length']!=other[f]['Length']:
                    results.append(self[f])
                else:
                    if self[f]['Digest']!=other[f]['Digest']:
                        results.append(self[f])
        # now make it a manifest
        if results==[]:
            return None
        else:
            r=checkmMF()
            r.append('Manifest Difference Between')
            for c in self.comments:
                r.append('M1:%s'%c)
            for c in other.comments:
                r.append('M2:%s'%c)
            for f in results:
                r[f['SourceFileOrURL']]=f                
            r.append(r.now())
            r.apsize()
            return r
        
    def __add__(self,other):
        ''' Aggregate comments and files '''
        for c in other.comments:
            if c not in self.comments:
                self.comments.append(c)
        for f in other.files:
            if f not in self.files:
                self[f]=other[f]
        return self
        
    def checksizes(self,remoteDir=None):
        ''' Check that the size of the files in the manifest matches the
        size of the files on disk, and return a list of any files which
        are not the right size.  (The primary use case for this method
        is to pass a remote directory and do a lightweight check on whether
        or not all the files have transferred ... if they have, then a
        full validation might follow ... if not, the incomplete/missing
        files could be resent.) '''
        incomplete=[]
        if remoteDir is None:
            for f in self.files:
                try:
                    if not self[f].validate(fromDisk=True,sizeOnly=True):
                        print 'Incomplete ',f
                        incomplete.append(f)
                except ValueError:
                    # file doesn't exist
                    print 'Missing ',f
                    incomplete.append(f)
        else: 
            # in this case we cannot use the internal checkmFile method
            if not os.path.exists(remoteDir):
                raise ValueError('Directory %s does not exist'%remoteDir)
            fileList=[(f,os.path.join(remoteDir,f)) for f in self.files]
            for k,f in fileList:
                if os.path.exists(f):
                    s1=len(self[k])
                    s2=os.path.getsize(f)
                    if s1!=s2:
                        print 'Incomplete ',f,k,s2,' should be ',s1
                        incomplete.append(f)
                else:
                    print 'Missing ',f
                    incomplete.append(f)    
        return incomplete
    
    def write(self,ofilename):
        ''' Write a manifest file out to disk '''
        self.comments.append(self.now())
        self.apsize()
        cols=[COLUMNS[i] for i in range(6)]
        self.comments.append('|%s|'%'|'.join(cols))
        of=file(ofilename,'w')
        of.write(str(self))
        of.close()
    
    def read(self,ifilename):
        ''' Read a manifest file from disk '''
        self.__emptyme()
        inf=file(ifilename,'r')
        lines=inf.readlines()
        inf.close()
        if lines==[]:
            raise ValueError('Empty manifest file')
        # skip the input manifest line ... 
        line1=lines[0].strip('\n')
        if self.comments[0]!=line1[1:]:
            if line1.startswith('%checkm'):
                print 'Updating manifest version from %s to %s'%(line1,self.comments[0])
            else:
                raise ValueError('Unrecognised manifest format, starts with \n %s'%line1)
        for line in lines[1:]:
            if line[0]=='#':
                self.append(line.strip('\n')[1:])
            else:
                cf=checkmFile(line=line)
                self[cf['SourceFileOrURL']]=cf
                
    def now(self):
        ''' Get a sensible timestamp'''
        ts=time.time()
        now=datetime.datetime.fromtimestamp(ts)
        return now.isoformat()
        
    def getInputDir(self):
        ''' Parse the comments and return an input directory, if one exists,
        otherwise None'''
        for c in self.comments:
            lookingfor='#Input Directory: '
            if c.startswith(lookingfor):
                idir=c.lstrip(lookingfor)
                return idir
        return None
        
    def subset(self,globstring):
        ''' Return a complete manifest corresponding to the appropriate subset
        of this manifest which is described by the glob string '''
        #get the matches
        results=[]
        for f in self.files:
            if fnmatch.fnmatchcase(f,globstring): results.append(f)
        #build a new manifest
        r=checkmMF()
        for c in self.comments: r.append('M1:%s'%c)
        r.append('M1 Manifest Subsetted to %s'%globstring)
        for f in results:
            r[f]=self[f]
        r.append(r.now())
        r.apsize()
        return r
            
class checkmManifest(file):
    ''' A complete checkm manifest. Comments can be added
     at any time, and are written first when the manifest is
     closed, then the manifest lines are written in the order
     received. 
     
     OBSOLETE
     
     '''
    def __init__(self,*args,**kwargs):
        ''' Instantiate like any other file '''
        file.__init__(self,*args,**kwargs)
        self.noComment=True
        self.noContent=True
        if self.mode=='w':
            self.noComment,self.noContent=False,False
            self.comments=['%checkm_0.7']
            self.content=[]
        elif self.mode=='r':
            self.comments=[]
            self.content=[]
            self._readSelf()
        elif self.mode=='a':
            self.noContent=False
            self.content=[]
            
    def _readSelf(self):
        lines=self.readlines()
        for line in lines:
            if line[0]=='#':
                self.comments.append(line[1:].strip('\n'))
            else:
                self.content.append(checkmFile(line=line))
            
    def addComment(self,line):
        ''' Comments are collected and added at the top '''
        if not self.noComment:
            self.comments.append(line)
            
    def addContent(self,chkfile):
        ''' Add a file to content by parsing the file from disk '''
        if self.mode=='w':
            self.content.append(checkmFile(filename=chkfile))
        else:
            c=checkmFile(filename=chkfile)
            self.write(str(c)+'\n')
            
    def addCheckmFile(self,chkfile,stripPath=True):
        ''' Add a file by using a completely instantiated checkm file '''
        if self.mode=='w':
            if stripPath:
                p,f=os.path.split(chkfile.tokens[0])
                chkfile.tokens[0]=f
            self.content.append(chkfile)
        else:
            self.write(str(c)+'\n')
            
    def appendComment(self,chkfile):
        ''' Add a comment to an existing checkmManifest'''
        # We need to open it in 'r'
        # mode, close it, and
        # then write it ...
        raise NotImplemented
        
    def now(self):
        ''' Get a sensible timestamp'''
        ts=time.time()
        now=datetime.datetime.fromtimestamp(ts)
        return now
        
    def mclose(self):
        if self.mode=='w':
            self.comments.append(self.now())
            cols=[COLUMNS[i] for i in range(6)]
            self.comments.append('|%s|'%'|'.join(cols))
            self.write(str(self))
        self.close()
        
    def __cmp__(self,other):
        if self.comments != other.comments: return False
        #order of data files doesn't matter.
        #assume same if file checksums match.
        i1={}
        i2={}
        for c in self.content:
            i1[c[0]]=c[2]
        for c in other.content:
            i2[c[0]]=c[2]
        for c in i1:
            if c not in i2: return False
            if i1[c]!= i2[c]: return False
        for c in i2:
            if c not in i1: return False
        return True
        
    def __str__(self):
        s=''
        for c in self.comments:
            s+='#%s\n'%c
        for c in self.content:
            s+=str(c)+'\n'
        return s
 
def getFilesFromManifest(maniFile,prependPath=None):
    ''' Get a list of files from a checkm manifest file (as a file
    or as an instance of a loaded manifest), and optionally
    turn into fully qualified paths by prepending a path '''
    if isinstance(maniFile,checkmMF):
        mf=maniFile
    else:
        mf=checkmMF()
        mf.read(maniFile)
    if prependPath is None:
        # attempt to find a path from manifest file
        idir=mf.getInputDir()
        if idir is not None:
            prependPath=idir
        else:
            raise ValueError('No prependPath provided and none found')
        
    prependPath=prependPath.strip('\n')
    return [os.path.join(prependPath,f) for f in mf.files]
    
def getFiles(fromFile,prependPath=None):
    ''' Get a list of files from a text file (one per line) or 
    from a manifest file (identified by extension .cmmf) '''
    
    if isinstance(fromFile,checkmMF):
        return getFilesFromManifest(fromFile,prependPath)
    
    assert os.path.isfile(fromFile)
    p,e=os.path.splitext(fromFile)
    if prependPath is not None: prependPath=prependPath.strip('\n')
    
    if e=='.cmmf':
        return getFilesFromManifest(fromFile,prependPath)
    else:
        f=file(manifestFile,'r')
        lines=f.readlines()
        if prependPath is not None:
            return [os.path.join(prependPath,line.strip('\n')) for line in lines]
        else:
            return [line.strip('\n') for line in lines]

class TestChecktools(unittest.TestCase):
    
    def setUp(self):
        ''' Setup temporary files '''
        self.f1='checkm.py'
        self.d1=os.getcwd()
        self.pyfiles=glob.glob('*.py')
        ntmpfiles=4
        self.tmpfiles=[]
        for i in range(ntmpfiles):
            tmpfile=tempfile.NamedTemporaryFile(suffix='.cmmf',delete=False)
            self.tmpfiles.append(tmpfile.name)
            tmpfile.close()
        self.testComments=['This is a test manifest','With two comment lines']
        
    def tearDown(self):
        ''' Get rid of temporary files'''
        for f in self.tmpfiles:
            if os.path.exists(f): os.remove(f)
        
    def _makeManifest(self,i):
        ''' Makes an old manifest instance for comparison, using the ith available name '''
        mf=checkmManifest(self.tmpfiles[i],'w')
        for c in self.testComments:
            mf.addComment(c)
        for f in self.pyfiles:
            mf.addContent(f)
        mf.mclose()
        return mf
        
    def _makeNewManifest(self,i):
        ''' Makes a new manifest instance, using ith available name '''
        mf=checkmMF(initialComments=self.testComments)
        for f in self.pyfiles: mf.add(f)
        mf.write(self.tmpfiles[i])
        return mf
        
    def _makeShortManifest(self,i):
        ''' Makes a short manifest, using ith available name '''
        mf=checkmMF(initialComments=self.testComments)
        for f in self.pyfiles[0:-2]: mf.add(f)
        mf.write(self.tmpfiles[i])
        return mf
        
    def _makeCorruptManifestByLength(self,i):
        ''' Makes a corrupt manifeset, using ith available name '''
        mf=checkmMF(initialComments=self.testComments)
        for f in self.pyfiles: mf.add(f)
        cfn=mf.files[0]
        cf=mf[cfn]
        cf['Length']=len(cf)+10
        mf[cfn]=cf
        mf.write(self.tmpfiles[i])
        return mf
        
    def _makeCorruptManifestByDigest(self,i):
        ''' Makes a corrupt manifeset, using ith available name '''
        mf=checkmMF(initialComments=self.testComments)
        for f in self.pyfiles: mf.add(f)
        cfn=mf.files[0]
        cf=mf[cfn]
        ed=cf['Digest']
        corrupt=ed[5:]
        cf['Digest']=corrupt
        mf[cfn]=cf
        mf.write(self.tmpfiles[i])
        return mf
        
    def testLine(self):
        c=checkmFile(filename=self.f1)
        
    def testString(self):
        c=checkmFile(filename=self.f1)
        s=str(c)
        c2=checkmFile(line=s)
        
    def testLength(self):
        c=checkmFile(filename=self.f1)
        assert isinstance(len(c),int),'Length of file not an integer'
        assert len(c)>0,'Length of file should be > 0 bytes'
        
    def testTokens(self):
        c=checkmFile(filename=self.f1)
        tokens=str(c).split('|')[1:-1]
        c2=checkmFile(tokens=tokens)
    
    def testValidate(self):
        c=checkmFile(filename=self.f1)
        assert c.validate(fromDisk=True)
        
    def testGetItem(self):
        c=checkmFile(filename=self.f1)
        self.assertEqual(c['SourceFileOrURL'],self.f1)
        
    def testManifestW(self):
        ''' test manifest writing'''
        self._makeManifest(0)
        
    def testManifestR(self):
        ''' test manifest reading '''
        mf1=self._makeManifest(0)
        mf2=checkmManifest(self.tmpfiles[0],'r')
        self.assertEqual(mf1,mf2)
        for f in mf2.content:
            assert f.validate(fromDisk=True)
        
    def testManifestAff(self):
        ''' test adding to an existing manifest '''
        mf1=self._makeNewManifest(0)
        mf2=self._makeShortManifest(1)
        print mf1.files
        print mf2.files
        mf3=mf1-mf2
        mf4=mf2+mf3
        print mf4.files
        self.assertEqual(mf1,mf4)
        
    def testNewManifestW(self):
        ''' test new manifest writing'''
        self._makeNewManifest(0)
        
    def testNewManifestR(self):
        ''' test new manifest reading '''
        self._makeNewManifest(0)
        m=checkmMF()
        m.read(self.tmpfiles[0])
        
    def testOldAndNew1to2(self):
        ''' test old and new interop, write with old, read with new '''
        mf1=self._makeManifest(0)
        mf2=checkmMF()
        mf2.read(self.tmpfiles[0])
        self.assertEqual(str(mf1),str(mf2))
        
    def testOldAndNew2to1(self):
        ''' test old and new interop, write with new, read with old '''
        mf1=self._makeNewManifest(0)
        mf2=checkmManifest(self.tmpfiles[0],'r')
        self.assertEqual(str(mf1),str(mf2))
    
    def testManiDif1(self):
        ''' Test differencing of manifests by size '''
        mf1=self._makeNewManifest(0)
        mf2=self._makeCorruptManifestByLength(1)
        mf3=mf1-mf2
        self.assertEqual(len(mf3.files),1)
        self.assertNotEqual(mf1,mf2)
        
    def testManiDif2(self):
        ''' test differencing of manifests by digest '''
        mf1=self._makeNewManifest(0)
        mf2=self._makeCorruptManifestByDigest(1)
        mf3=mf1-mf2
        self.assertEqual(len(mf3.files),1)
        self.assertNotEqual(mf1,mf2)
        
    def testManiSubset(self):
        ''' Test subsetting '''
        mf1=self._makeNewManifest(0)
        mf2=mf1.subset('c*.py')
        mf3=mf1-mf2
        print mf2
        assert len(mf2)+len(mf3)==len(mf1)
        
if __name__=="__main__":
    unittest.main()

    
    
        
    
    
