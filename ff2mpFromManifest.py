#!/usr/bin/env python
import sys
import os
import uuid
from convertDir import convertFile
import stat
from simpleThreader import doit


if __name__=="__main__":
    # get the manifest
    try:
        manifest=sys.argv[1]
        destDir=sys.argv[2]
        streams=int(sys.argv[3])
        outManifest=sys.argv[4]
    except:
        print '''
        Usage: python ff2mpFromManifest inManifest destDir streams outManifest       
        '''
        raise
        exit()
        
    #get a list of files to work with
    ff=open(manifest,'r')
    files=[f.strip('\n') for f in ff.readlines()]
    
    #sort out the destination
    if not os.path.exists(destDir):
        os.makedirs(destDir)
        assert os.path.isdir(destDir)
    
    #create a unique base for the jobs
    ufname=str(uuid.uuid4())
    count=0
    
    print 'Processing ',files
    
    count=range(len(files))
    args=[[files[i],destDir,i,ufname,outManifest] for i in count]
    
    doit(convertFile,args,nstreams=streams)
    
    
