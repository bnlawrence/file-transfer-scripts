#!/usr/bin/env python2.7
import os
import sys
from time import sleep,time,gmtime,strftime
from datetime import datetime
import numpy as np
import glob

#avoid needing an x-connection
import matplotlib
matplotlib.use('Agg')

from matplotlib.dates import DateFormatter,MinuteLocator

from ftutils import kB,MB,GB,TB,KiB,MiB,GiB,TiB
from ftutils import humanBW,humanSize,bwtovpd
from checkm import checkmMF

import matplotlib.pyplot as plt

class monitor(object):
    ''' Class to support monitoring the change in size (growth) of a set of files
    as they are produced, includes reporting and plotting. '''
    def __init__(self,templateOrManifestOrDirectory,interval=15,plot_interval=4,logfile='logfile.txt'):
        ''' 
        Monitors one of the following:
			- a specific set of files passed a globtemplate, or
			- a specific set of files listed in a manifest, or
			- a directory
		(which is/are expected to be growing) and reports total growth 
		rate in appropriate units every <interval> seconds 
		(and plot every <plot_interval> times the interval).
		'''
        gstring='starting'
        # decide on route for calculations:
        self.target=templateOrManifestOrDirectory
        if os.path.isdir(self.target):
            useDir=1
        else: 
            useDir=0
		
		# initial size:
        if useDir:
            size=self._getdirSize(self.target)
        else: 
            size=self._getsizes(self.target)
			
        #initialise
        t1=time()
        t0=t1
        originalSize=size
        oldSize=-1
        self.data=[[],[],[]]
        count=0
        self._loadold(logfile)
        bufsize=1 # line buffered
        self.lf=open(logfile,'a',bufsize)
        notExcessiveWaiting=True
        excessiveWait=600 #s
        waitCount=0
        
        while (size <> oldSize and notExcessiveWaiting):
            # get bandwidth, current and lifetime (total)
            oldSize=size
            sleep(interval)
            if useDir:
                size=self._getdirSize(self.target)
            else:
                size=self._getsizes(self.target)
            if size>oldSize:
                t2=time()
                dt=t2-t1
                tdt=t2-t0
                t1=t2
                cdsize=size-oldSize
                tdsize=size-originalSize
                cbw,tbw=cdsize/dt,tdsize/tdt
                # how many different ways can one do this ...
                date=strftime("%Y-%m-%d %H:%M:%S", gmtime())
                gstring=bwtovpd(tbw)
                self.pretty(cbw,tbw,date,gstring)
                self.data[0].append(datetime.fromtimestamp(t2))
                self.data[1].append(cbw)
                self.data[2].append(tbw)
                count+=1
                if count%plot_interval==0: self.plot(gstring,size)
            else: 
                waitCount+=1
                if waitCount*interval>excessiveWait: notExcessiveWaiting=False
            
        print 'Files have stopped growing'
        self.plot(gstring,size,finished=True)
        
    def _loadold(self,logfile):
        ''' Check for older data from logfile, and get that first '''
        scaler={'kB':kB,'MB':MB,'GB':GB}
        if os.path.exists(logfile):
            lf=open(logfile,'r')
            for line in lf.readlines():
                sdate,bstring=line.split('b/w')
                d=datetime.strptime(sdate,'%Y-%m-%d %H:%M:%S ')
                tokens=bstring.split(' ')
                d1=float(tokens[2].rstrip(','))
                d2=float(tokens[4])
                s=tokens[5].strip('(/s)')
                if s in scaler:
                    scale=scaler[s]
                else: scale=1.
                self.data[0].append(d)
                self.data[1].append(d1*scale)
                self.data[2].append(d2*scale)
                print [self.data[i][-1] for i in [0,1,2]]
            lf.close()
        
    def pretty(self,cbw,tbw,date,gstring):
        ''' Pretty output of bandwidths as a string, and 
        get scaling and units for printing and plotting'''
        # convert to human readable units
        self.scale=1
        transition=2000
        if cbw<transition:
            units='B/s'
        elif cbw<kB*transition:
            units='kB/s'
            cbw=cbw/kB
            tbw=tbw/kB
            if self.scale < kB: self.scale=kB
        elif cbw<MB*transition:
            units='MB/s'
            cbw=cbw/MB
            tbw=tbw/MB
            if self.scale < MB: self.scale=MB
        elif cbw<GB*transition:
            units='GB/s'
            cbw=cbw/GB
            tbw=tbw/GB
            if self.scale < GB: self.scale=GB
        
        self.units=units
        logstring='%s b/w: Current: %.3f, Total %.3f (%s) (%s)'%(date,cbw,tbw,units,gstring)
        print logstring
        self.lf.write(logstring+'\n')
        
    def _sizer(self,size):
        ''' Resize file size and return as string'''
        s,u=humanSize(size)
        return '%s %s'%(s,u)
        
    def plot(self,gstring,size,finished=False):
        ''' Plot the internal data timeseries '''
        # (I think I) need to use the subplot incantation to use the 
        # xdate autoformatter
        fig=plt.figure()
        thisplt=fig.add_subplot(1,1,1)
        # page title
        if len(self.filenames)==1:
            tstring=self.filenames[0]
        else: tstring='%s (%s files)'%(self.target,len(self.filenames))
        if finished:
            tstring+='\n Completed(?) size %s'%self._sizer(size)
        else: 
            tstring+='\n Current size %s'%self._sizer(size)
        plt.title(tstring)
        # matplotlib is smart about python datetime instances:
        x=self.data[0]
        fig.autofmt_xdate()
        plt.ylabel(self.units)
        # plot and overplot
        thisplt.plot(x,np.array(self.data[1])/self.scale)
        y2=np.array(self.data[2])/self.scale
        thisplt.plot(x,y2)  
        # annotate the final rate per day
        thisplt.annotate(gstring,xy=(x[-1],y2[-1]),horizontalalignment='right')
        plt.savefig('incoming-bw.png')
        # note that if we don't close the file, the plot is added over
        plt.close()
        
    def _getsizes(self,templateOrManifest):
        ''' Returns total size on file system of files described by template
        or manifest file '''
        self.filenames=[]
        # consider strings of globs separated by a ','
        if '*' in templateOrManifest:
            templates=templateOrManifest.split(',')
            for t in templates:
                self.filenames+=glob.glob(t)
        else: #assume a manifest
            mf=checkmMF()
            mf.read(templateOrManifest)
            # now make sure that we only look for files that we've already
            # started getting
            base=os.path.split(templateOrManifest)
            for f in mf.files:
                ff=os.path.join(base[0],f)
                if os.path.exists(ff):self.filenames.append(ff)
        size=0
        for f in self.filenames:
            assert os.path.isfile(f)
            size+=os.path.getsize(f)
        return size
        
    def _getdirSize(self,dirName):
        ''' Walks a directory and gets total size of contents '''
        size = 0
        self.filenames=[]
        for dirpath, dirnames, filenames in os.walk(dirName):
            for f in filenames:
                fp = os.path.join(dirpath, f)
                size += os.path.getsize(fp)
                self.filenames.append(fp)
        return size
		

if __name__=="__main__":
    try:
        assert len(sys.argv)==2
    except:
        print "Usage: python monitor 'blah*.txt'"
    x=monitor(sys.argv[1],interval=15,plot_interval=20)

