#!/usr/bin/env python
# Simple parallelisable script for creating a local checksum
# manifest to match a remote checksum manifest. Suitable for 
# small lists of files. Use jugable version for big jobs.

from checkm import checkmMF,checkmFile
from simpleThreader import doit 
import time,datetime
import socket

def MakeCheckmFile(filename):
    ''' This is a simple wrapper to checkm which exists so it can
    be parallelised '''
    x=checkmFile(filename=filename)
    return x

def getMFfor(remoteMF,nstreams=3):
    ''' Create a local manifest to compare with a remote manifest,
    with nstreams levels of parallelisation '''
    # Load remote manifest
    rmf=checkmMF()
    rmf.read(remoteMF)
    
    # May need to strip pathnames ... 

    # get going now
    print 'Need to create checkm information for '
    print rmf.files
    
    print ''
    
    localFiles=doit(MakeCheckmFile,rmf.files,nstreams)
 
    return localFiles,rmf

def finaloutput(remoteManifest,rmf,outputs,localManifest):
    ''' This writes out the final results '''
    lmf=checkmMF()
    lmf.append('Manifest created for files in Remote Manifest %s'%remoteManifest)
    lmf.append('Original Manifest Comments Follow')
    for c in rmf.comments[1:]:
        lmf.append('->%s'%c)
    lmf.append('Local Manifest Comments Follow')
    lmf.append('Running on: %s'%socket.getfqdn())
    for f in outputs: lmf[f[0]]=f
    lmf.append(lmf.now())
    comment='Total volume of %s files in manifest: %s %s '%(len(lmf),lmf.hsize[0],lmf.hsize[1])
    print comment
    lmf.append(comment)
    lmf.write(localManifest)
    return lmf
    
if __name__=="__main__":
    
    import sys
    if len(sys.argv)==4:
        remoteMFname=sys.argv[1]
        localMFname=sys.argv[2]
        nstreams=sys.argv[3]
    else:
        print 'Usage: getLocalManifest.py remoteMFname newLocalMFname nstreams'
        exit()
    
    t1=time.time()
    localFiles,rmf=getMFfor(remoteMFname,nstreams=3)
    lmf=finaloutput(remoteMFname,rmf,localFiles,localMFname)
    t2=time.time()
    print 'Manifest size ',str(lmf.hsize),' produced in %s s'%str(t2-t1)
    
