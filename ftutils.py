## Basic File Transfer Units for monitoring etc

# not to be confused with their binary friends, 
# KiB, MiB, GiB, and TiB which are the 1024^x sequence
# bandwidths are measured in kB and friends, but 
# linux reports volumes in KiB and friends.

kB=1000
MB=kB*1000
GB=MB*1000
TB=GB*1000

KiB=1024
MiB=KiB*1024
GiB=MiB*1024
TiB=GiB*1024

def bwtovpd(bw):
    ''' Calculate GiB or TiB per day given a bandwidth in bytes/seconds'''
    gday=bw*86400/GiB
    tday=bw*86400/TiB
    if gday<1500:
        gstring='%.1f GiB/day'%gday
    else:
        gstring='%.2f TiB/day'%tday
    return gstring
    
def humanSize(size,scaler=2000):
    ''' Given a volume in Bytes, convert it to human units, and
    provide the units as a string '''
    for choice in [(1,'B'),(KiB,'KiB'),(MiB,'MiB'),(GiB,'GiB'),(TiB,'TiB')]:
        if size < scaler*choice[0]:
            return size/choice[0],choice[1]
    return size/TiB,'TiB'
            
def humanBW(bw,scaler=2000):
    ''' Given a bw in Bytes/s, convert it to human units, and
    provide the units as a string '''
    for choice in [(1,'B/s'),(kB,'kB/S'),(MB,'MB/s'),(GB,'GB/s'),(TB,'TB/s')]:
        if bw < scaler*choice[0]:
            return bw/choice[0],choice[1]
    return size/TB,'TB/s'        
            
def fileStats(filename,dochecksum=True):
    ''' Returns a tuple of important file stats: directory, name, size, checksum '''
    path=os.path.split(filename)
    size=os.path.getsize(filename)
    if dochecksum:
        cmdstr='sha1sum %s'%filename
        checkout=cmd2result(cmdstr)
        checksum=checkout.split(' ')[0]
        return path[0],path[1],size,checksum
    else:
        return path[0],path[1],size
        
def xmlFileStats(filename,server='archer'):
    ''' Prettify information about a file '''
    return '''<file><directory>%s</directory><name>%s</name>
              <size>%s</size><checksum>%s</checksum></file>
              '''%fileStats(filename,server)
    
def cmdExecute(cmdstr,logfile=None):
    ''' Error handling on execution of shell commands '''
    try:
        retcode=subprocess.call(cmdstr,shell=True)
        if retrcode <0:
            estring='[%s] terminated by %s'%(cmdstr,-retcode)
            print >> sys.strderr,estring
            if logfile is not None: logfile.write(estring+'\n')
    except OSError as e:
        estring='[%s] Execution failed %s'%(cmdstr,e)
        print >> sys.stderr, estring
        if logfile is not None: logfile.write(estring+'\n')
        
def cmd2result(cmdstr,logfile=None):
    ''' Execute a command in the shell, and return the result, with error handling'''
    try:
        return subprocess.check_output(cmdstr,shell=True)
    except subprocess.CalledProcessError as e:
        estring='[%s] execution failed %s'%(cmdstr,e)
        if logfile is not None: logfile.write(estring+'\n')
        raise

