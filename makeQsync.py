#!/usr/bin/env python
# Used to queue rsync jobs
import sys

qtemplate='''#!/bin/bash 
#
#PBS -l select=serial=true
#PBS -l walltime=%s:0:0
#PBS -A n02
#
# need files to be written in submitting directory (if no path specified)
cd $PBS_O_WORKDIR
#
mfile=%s
destDir=%s
streams=%s
date1=$(date +"%%s")
prsync.py $mfile $destDir $streams
date2=$(date +"%%s")
diff=$(($date2-$date1))
echo "prsync took ${diff}s." >> syncQueue.log
'''

def makeQfile(hours,mfile,destDir,streams):
    qj=qtemplate%(hours,mfile,destDir,streams)
    qfn='moving_%s.sh'%mfile
    qf=open(qfn,'w')
    qf.write(qj)
    qf.close()
    
if __name__=="__main__":
    try:
        mfile=sys.argv[1]
        destDir=sys.argv[2]
        streams=int(sys.argv[3])
        hours=int(sys.argv[4])
    except:
        print 'Usage: makeQsync.py mfile destDir streams hours'
        exit()
    makeQfile(hours,mfile,destDir,streams)


    

