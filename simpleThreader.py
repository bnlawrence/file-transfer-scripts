#
# Simple multiprocessing tool
# Provides methods to thread functions via a pool
#

from glob import glob
from time import time
import os
from multiprocessing.pool import Pool
from datetime import datetime
import sys
from copy import deepcopy

streamOutput=[]

def doitasync(infunc,arglist,nstreams=4):
    ''' Call a <infunc> len(arglist) times, using the members of
    arglist as the arguments to each call. Do this in a pool with <nstreams>
    workers. By default the output of each function call will be put into 
    streamOutput as a list item - but in an arbitrary order '''
    # this hasn't been tested, unlike the rsynctool version ...
    t0=time()
    pool=Pool(processes=streams)
    for i in arglist:
        pool.apply_async(rsyncit,args=i,callback=log_result)
    pool.close()
    pool.join()
    t1=time()
    return deepcopy(streamOutput)
    
def doit(infunc,arglist,nstreams=4):
    ''' Call a <infunc> len(arglist) times, using the members of
    arglist as the arguments to each call. Do this in a pool with <nstreams>
    workers. By default the output of each function call will be put into 
    streamOutput as a list item. '''
    t0=time()
    pool=Pool(processes=nstreams)
    results=pool.map(infunc,arglist)
    t1=time()
    print '%s function calls completed in %s seconds'%(len(arglist),t1-t0)
    return results
    
def log_result(aresult):
    # This is called whenever foo_pool(i) returns a result.
    # output list modified only by the main process, not the pool workers.
    streamOutput.append(aresult)    
    
def _rsyncit(args):
    ''' Synchronise args[0] to args[1] '''
    t0=time()
    os.system('rsync -a %s %s'%args)
    #print args
    t1=time()
    fs=os.path.getsize(args[0])
    return (datetime.fromtimestamp(t1).isoformat()[:19],args[0],fs,t1-t0)    
    
def _egrsync(template,destdir):
    ''' rsync files described by template into destdir '''
    files=glob(template)
    files.sort()
    args=[(f,destdir) for f in files]
    results=doit(_rsyncit,args)
    print results
    
if __name__=="__main__":
    
    ## Demonstrate usage by providing a simple rsync tool which uses
    ## multiple threads
    
    try:
        template=sys.argv[1]
        destDir=sys.argv[2]
    except:
        print "Usage: python rsynctool.py 'blah*' destdir/"
        exit()
        
    _egrsync(template,destDir)
