#
# Simple multiprocessed rsync tool
#


from glob import glob
from time import time
import os
from multiprocessing.pool import Pool
from datetime import datetime
import sys

streamOutput=[]

def rsyncit(instruction,cmd='rsync -av %s %s',log=False):
    t0=time()
    r=os.system(cmd%instruction)
    if log:
        t1=time()
        s=os.path.getsize(instruction[0]) 
        return [datetime.fromtimestamp(t1),instruction[0],s,t1-t0,r]
    return r

def rsynctool(template,destDir,streams=4):
    # simple no logging alternative
    ''' Given a template of files manage data transfers to destDir '''
    files=glob(template)
    instructions=[(f,destDir) for f in files]
    pool=Pool(processes=streams)
    pool.map(rsyncit,instructions)
    
def log_result(olist):
    # This is called whenever foo_pool(i) returns a result.
    # output list modified only by the main process, not the pool workers.
    streamOutput.append(olist[0:-1])

def rsynclog(template,destDir,streams=4):
    # more complicated choice, but gives results
    ''' Given a template of files manage data transfers to destDir '''
    t0=time()
    files=glob(template)
    instructions=[(f,destDir) for f in files]
    
    pool=Pool(processes=streams)
    for i in instructions:
        pool.apply_async(rsyncit,args=(i,'rsync -av %s %s',True),callback=log_result)
    pool.close()
    pool.join()
    t1=time()
    f=open('rsynclog.txt','wa')
    f.write('Moving %s files to %s using %s streams\n'%(len(files),destDir,streams))
    f.write('From %s to %s\n'%(datetime.fromtimestamp(t0),datetime.fromtimestamp(t1)))
    f.write('Total time taken: %s\n'%t1-t0)
    for r in streamOutput: 
        f.write(r+'\n')
    f.close()
    
if __name__=="__main__":
    try:
        template=sys.argv[1]
        destDir=sys.argv[2]
    except:
        print "Usage: python rsynctool.py 'blah*' destdir/"
        exit()
    #rsynctool(template,destDir)
    rsynclog(template,destDir)
