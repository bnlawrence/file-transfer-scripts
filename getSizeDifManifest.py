#!/usr/bin/env python

from checkm import checkmMF
import os

def getSizeDifManifest(manifestName,otherDir=None):
    ''' Return a manifest file containing a list of all files which fail
    a size test (locally or in another directory). This routine is used 
    for checking whether the files contained in a manifest are "all there" 
    It does not do checksums. '''
    f=checkmMF()
    f.read(manifestName)
    try:
        files=f.checksizes(remoteDir=otherDir)
    except ValueError,msg:
        print msg
        exit(1)
    if files==[]:
        return []
    else:
        g=checkmMF()
        g.append(' Incorrect File Size Manifest')
        g.append(' A list of files from %s'%manifestName)
        ext=''
        if otherDir is not None: ext=' at %s'%otherDir
        g.append(' Which are not on disk %s, or incomplete'%ext)
        g.append(' Original Manifest comments follow')
        for c in f.comments: g.append('-> %s'%c)
        for ff in files:
            p,k=os.path.split(ff) 
            g[k]=f[k]
        return g

if __name__== "__main__":
    
    import sys
    
    if len(sys.argv)==4:
        mfn=sys.argv[1]
        nmf=sys.argv[2]
        odir=sys.argv[3]
    elif len(sys.argv)==3:
        mfn=sys.argv[1]
        nmf=sys.argv[2]
        odir=None
    else:
        print 'Usage: getSizeDifManifest.py inManiName outManiName [otherDir] '
        exit(1)
    try:
        g=getSizeDifManifest(mfn,otherDir=odir)
    except ValueError,msg:
        print msg
        exit(1)
    if g==[]:
        print 'Congratulations: Nothing missing, no manifest created'
        exit()
    g.write(nmf)
    
        
    
    
