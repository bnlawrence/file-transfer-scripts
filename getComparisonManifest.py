#!/usr/bin/env python

from checkm import checkmMF

def getComparisonManifest(originalManifest,supposedEquivalentManifest):
     ''' This routine is used for checking that the files described
     by two manifests are the same, if not, it returns the difference
     as a manifest. '''
     mf1=checkmMF()
     mf2=checkmMF()
     mf1.read(originalManifest)
     mf2.read(supposedEquivalentManifest)
     return mf1-mf2
     
if __name__=="__main__":

    import sys
    
    if len(sys.argv)==4:
        omf=sys.argv[1]
        emf=sys.argv[2]
        nmf=sys.argv[3]
    else:
        print 'Usage: originalManifest supposedEquivalentManifest outputDifferenceManifest '
        exit()
    g=getComparisonManifest(omf,emf)
    if g is None:
	print 'No difference found'
    else:    
	g.write(nmf)
    
