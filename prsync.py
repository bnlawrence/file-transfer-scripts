#!/usr/bin/env python
# rsync directories using a manifest and threading
from simpleThreader import doit
import os
import sys
from time import time
from datetime import datetime
from checkm import checkmManifest,getFilesFromManifest, getFiles

def rsync(args,cmd='cp'):
    ''' Synchronise args[0] to args[1] '''
    t0=time()
    if cmd=='cp':
	cmd='cp %s %s'%args
    else:
	cmd='rsync -a -P %s %s'%args
    print cmd
    os.system(cmd)
    t1=time()
    fs=os.path.getsize(args[0 ])
    r=[datetime.fromtimestamp(t1).isoformat()[:19],args[0],fs,t1-t0]
    print r
    return r

if __name__=="__main__":
    try:
        manifestFile=sys.argv[1]
        sourceDir=sys.argv[2]
        targetDir=sys.argv[3]
        if len (sys.argv)>4: 
            streams=int(sys.argv[4])
            print 'Using %s streams'%streams
        else: streams=4
    except:
        print 'Usage: prsync manifestFile sourceDir targetDir'
        exit()
    files=getFiles(manifestFile,prependPath=sourceDir)
    args=[(f,targetDir) for f in files]
    print 'Executing in parallel ',args
    r=doit(rsync,args,nstreams=streams)
    print 'Finished parallel execution'
    print r
    
